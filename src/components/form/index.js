import URL from 'url';
import { Component } from 'preact';
import hs from 'preact-hyperstyler';
import linkstate from 'linkstate';
import { route } from 'preact-router';
import archetypes from '../../data/archetypes';
import config from '../../../config.json';
import styles from './style.styl';

const h = hs(styles);

export default class Form extends Component {
	componentWillMount() {
		this.redirectUrl = location.protocol + '//' + location.host + '/reading';
	}

	componentDidMount() {
		this.props.componentDidMount(this.formEl);
	}

	render() {
		const funnelRef = this.props.urlQuery.ref || 'facebook';
		const aweberData = Object.assign({}, archetypes[this.props.quizData.archetype].aweber.default, archetypes[this.props.quizData.archetype].aweber[funnelRef]);
		console.log('FORM >>>>>>>>>>>>>>>>>>', this.props);
		return h.div('.container', [
			h.form({
				onSubmit: e => {
					e.preventDefault();
					fetch(`http://api.maropost.com/accounts/1230/lists/15/contacts.json?auth_token=${config.maropost.auth_token}`, {
						method: 'POST',
						mode: 'no-cors',
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json',
						},
						body: JSON.stringify({
							contact: {
								email: this.props.quizData.email,
								custom_field: { /* eslint id-match: 0 */
									name: 'ka',//this.props.quizData.name,
									gender: 'male', //this.props.quizData.gender,
									birthday: '2017-01-01', //this.props.quizData.birthday,
									horoscope: 'libra',
									element: 'air',
									archetype: 'lover'
								}
							}
						})
					}).then(() => {
						this.props.onSubmit(this.props.quizData);
					});
					return;

					// const http = new XMLHttpRequest();
					// http.mode = 'no-cors';
					// const url = `http://api.maropost.com/accounts/1230/lists/15/contacts.json?auth_token=${config.maropost.auth_token}`;
					// const params = {
					// 	contact: {
					// 		email: this.props.quizData.email,
					// 		custom_field: { /* eslint id-match: 0 */
					// 			name: 'ka',//this.props.quizData.name,
					// 			gender: 'male', //this.props.quizData.gender,
					// 			birthday: '2017-01-01', //this.props.quizData.birthday,
					// 			horoscope: 'libra',
					// 			element: 'air',
					// 			archetype: 'lover'
					// 		}
					// 	}
					// };
					// this.props.onSubmit(this.props.quizData);
					// // return;

					// http.open('POST', url, true);
					// http.setRequestHeader('Content-Type', 'application/json');
					// http.setRequestHeader('Accept', 'application/json');
					// http.onreadystatechange = () => {
					// 	console.log(http.responseText);
					// 	if (http.readyState === XMLHttpRequest.DONE && http.status === 201) {
					// 		this.props.onSubmit(params);
					// 	} else {
					// 		// Error case
					// 		/* Temporary */
					// 		this.props.onSubmit(params);
					// 	}
					// };

					// http.send(JSON.stringify(params));
					// console.log('@@@@@@@@@@@@@@@@@@@@', this.formEl, params);
				},
				// action: `http://api.maropost.com/accounts/1230/lists--/${aweberData.listname}/contacts.json?auth_token=${config.maropost.auth_token}`,
				// method: 'POST',
				ref: ref => {
					this.formEl = ref;
				},
			}, [
				h.input({
					type: 'name',
					name: 'name',
					value: this.props.quizData.name,
					placeholder: 'Name',
					onchange: linkstate(this, 'name'),
					required: true,
				}),
				h.input({
					type: 'email',
					name: 'email',
					placeholder: 'Email',
					value: this.props.quizData.email,
					onchange: linkstate(this, 'email'),
					required: true,
				}),
				h.button('Start My Free Reading!'),
			])
		]);
	}
}