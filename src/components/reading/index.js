import { Component } from 'preact';
import { route } from 'preact-router';
import hs from 'preact-hyperstyler';
import component from '..';
import styles from './style.styl';

const h = hs(styles);

export default class extends Component {
  render() {
    if ('deluxe' in this.props.urlQuery) {
      return route('/deluxe?' + this.props.urlQuery.deluxe);
    }

    if (this.error) {
      return h.pre(this.error);
    }

    console.log('reading/index.js', this.props);
    return h.div('.wrapper.reading', [
      h(component('reading-base'), {
        audioName: this.props && this.props.quizData && this.props.quizData.archetype,
        onended: () => route('/deluxe'),
        ...this.props,
      }),
    ]);
  }

}
