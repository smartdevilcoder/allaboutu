import {
	Component
} from 'preact';
import {
	route
} from 'preact-router';
import hs from 'preact-hyperstyler';
// import throttle from 'throttleit';
// import archetypes from '../../data/archetypes';
// import questions from '../../data/questions';
import getcomponent from '..';
import styles from './style.styl';

const h = hs(styles);

export default class Quiz extends Component {
	render() {
		const redirecting = this.props.redirect();
		if (redirecting) {
			return redirecting;
		}

		return h.div({class: ['quiz', 'wrapper']}, [h.div('.container', [
			getcomponent('header'),
			h(require('./form/index.js').default, {onFinish: this.props.onFinish})
		])]);
	}
}
