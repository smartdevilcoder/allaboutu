import {
	Component
} from 'preact';
import hs from 'preact-hyperstyler';
import styles from './style.styl';

const h = hs(styles);

export default class extends Component {
	constructor(props) {
		super(props);
		this.setData = props.setData;
		this.setElement = props.setElement;
		this.updateEmail = this.updateEmail.bind(this);
		console.log('********************* Step', this.state, props);
	}

	updateEmail(e) {
		this.setData({email: e.target.value});
	}

	componentDidMount() {
		this.setElement('email', this.email);
		this.email.focus();
		console.log('MOUNTED-----------', this.email);
	}

	render() {
		return h.div('.step', [
			h.div([
				h.input('.email', {
					name: 'email',
					type: 'email',
					placeholder: 'Enter Your Email...',
					onchange: this.updateEmail,
					ref: ref => {
						this.email = ref;
					}
				})
			])
		]);
	}
}
