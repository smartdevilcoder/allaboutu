import { Component } from 'preact';
import hs from 'preact-hyperstyler';
import styles from './style.styl';

const h = hs(styles);
const steps = ['step1', 'step2', 'step3'].map((step) => require('../' + step + '/index.js').default);
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const horoscopeMap = {
	'0120-0218': ['Aquarius', 'Air', 'magician', 'outlaw'],
	'0219-0319': ['Pisces', 'Water', 'creator', 'innocent'],
	'0321-0419': ['Aries', 'Fire', 'ruler', 'hero'],
	'0420-0520': ['Taurus', 'Earth', 'hero', 'lover'],
	'0521-0620': ['Gemini', 'Air', 'member', 'member'],
	'0621-0722': ['Cancer', 'Water', 'cravegiver', 'cravegiver'],
	'0723-0822': ['Leo', 'Fire', 'jester', 'ruler'],
	'0823-0922': ['Virgo', 'Earth', 'innocent', 'creator'],
	'0923-1022': ['Libra', 'Air', 'lover', 'innocent'],
	'1023-1121': ['Scorpio', 'Water', 'outlaw', 'magician'],
	'1122-1221': ['Sagittarius', 'Fire', 'explorer', 'jester'],
	'1222-0119': ['Capricorn', 'Earth', 'sage', 'sage']
};

const calculateHoroscopeDetails = (data) => {
	const yyyymmdd = data.birthday.split('-');
	const monthday = `${yyyymmdd[1]}${yyyymmdd[2]}`;
	let value = null;
	let min = null;
	let max = null;

	value = Object.keys(horoscopeMap).find( (range) => {
		[min, max] = range.split('-');
		return monthday >= min && monthday <= max;
	});
	value = horoscopeMap[value];
	return Object.assign({
		horoscope: value[0],
		element: value[1],
		archetype: value[ data.gender === 'male' ? 2 : 3]
	}, data);
};

export default class extends Component {
	constructor (props) {
		super(props);
		this.state = {
			currentStep: 0,
			gender: 'male'
		};
		this.onFinish = props.onFinish;
		this.refs = {};
		this.formSubmit = this.formSubmit.bind(this);
		this.setData = this.setData.bind(this);
		this.setElement = this.setElement.bind(this);
	}

	validate() {
		if (this.state.currentStep === 0 && !this.state.name) {
			this.error = 'Name cannot be empty !';
			this.refs.name.focus();
			return false;
		}

		if (this.state.currentStep === 1 && !this.state.birthday) {
			this.error = 'Invalid birthday !';
			this.refs.birthday.focus();
			return false;
		}

		if (this.state.currentStep === 2 && !emailRegex.test(this.state.email)) {
			this.error = 'Invalid email !';
			this.refs.email.focus();
			return false;
		}

		this.setState({error: null});
		return true;
	}

	setElement(name, elem) {
		this.refs[name] = elem;
	}

	setData(data) {
		this.setState(Object.assign(this.state, data));
	}

	formSubmit(e) {
		e.preventDefault();
		if (this.validate()) {
			if (this.state.currentStep === 2) {
				const data = Object.assign({archetype: 'lover'}, calculateHoroscopeDetails(this.state));
				delete data.currentStep;
				delete data.error;
				this.onFinish(data);
			} else {
				this.setState({error: null, currentStep: this.state.currentStep + 1});
			}
		} else {
			this.setState({error: this.error});
			setTimeout(() => console.log('Validation Error', this.state));
		}
		console.log(e, this.state);
	}

	render() {
		return h.form({onsubmit: this.formSubmit}, [
			h.h2(`Step ${this.state.currentStep + 1}`),
			h.div('.seperator', ''),
			h(steps[this.state.currentStep], {
				setData: this.setData,
				setElement: this.setElement
			}),
			h.div([
				h.button('.continue', {type: 'submit'}, [
					h.span('Continue...'),
					h.span('⟩')
				])
			]),
			this.state.error && h.div('.error', this.state.error)
		]);
	}
}
