import {
	Component
} from 'preact';
import hs from 'preact-hyperstyler';
import styles from './style.styl';

const h = hs(styles);

export default class extends Component {
	constructor(props) {
		super(props);
		this.setData = props.setData;
		this.setElement = props.setElement;
		this.updateName = this.updateName.bind(this);
		this.updateGender = this.updateGender.bind(this);
		console.log('********************* Step', this.state, props);
	}
	/*
	getCurrentStep() {
			return null;
		// const Step = steps[this.state.currentStep];
		//return h.Step() setData={this.appendStepDetails}/>;
	}

	
	*/
	updateGender(e) {
		this.setData({gender: e.target.value});
	}

	updateName(e) {
		this.setData({name: e.target.value});
	}

	componentDidMount() {
		this.setElement('name', this.name);
		console.log('MOUNTED-----------', this.name);
	}

	render() {
		return h.div('.step', [
			h.div([
				h.input('.name', {
					name: 'name',
					autofocus: true,
					type: 'text',
					placeholder: 'Enter Your Name...',
					onchange: this.updateName,
					ref: ref => {
						this.name = ref;
					}
				})
			]),
			h.div([
				h.select('.gender', {
					name: 'gender',
					placeholder: 'Enter Your Gender...',
					onchange: this.updateGender
				}, [
					h.option('Male', {value: 'male'}),
					h.option('Female', {value: 'female'})
				])
			])
		]);
		/*
			return h.form( {onsubmit: () => {}}, [
				h.h1('Step 1'),
				h.div(stepNumber)
			]);
		*/
	}
}
