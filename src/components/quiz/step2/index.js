import {
	Component
} from 'preact';
import hs from 'preact-hyperstyler';
import styles from './style.styl';

const h = hs(styles);

const getMinBirthday = () => {
	const d = new Date();
	const year = d.getFullYear() - 1;
	let month = String(d.getMonth() + 1);
	let day = String(d.getDate());
	if (month.length < 2) {
		month = '0' + month;
	}
	if (day.length < 2) {
		day = '0' + day;
	}
	return [year, month, day].join('-');
}

export default class extends Component {
	constructor(props) {
		super(props);
		this.setData = props.setData;
		this.setElement = props.setElement;
		this.updateBirthday = this.updateBirthday.bind(this);
		console.log('********************* Step', this.state, props);
	}

	updateBirthday(e) {
		this.setData({birthday: e.target.value});
	}

	componentDidMount() {
		this.setElement('birthday', this.birthday);
		this.birthday.focus();
		console.log('MOUNTED-----------', this.birthday);
	}

	render() {
		return h.div('.step', [
			h.div([
				h.input('.birthday', {
					name: 'birthday',
					type: 'date',
					min: '1950-01-01',
					max: getMinBirthday(),
					placeholder: 'Enter Your Date Of Birth...',
					onchange: this.updateBirthday,
					ref: ref => {
						this.birthday = ref;
					}
				})
			])
		]);
	}
}
