import hs from 'preact-hyperstyler';
import logo from '../../assets/images/logos/individualogist-logo2.png';
import styles from './style.styl';

const h = hs(styles);

export default h.header([
	h.div('.logo', [
		h.img({ src: logo })
	]),
	h.div('.header-text', [
		h.h1('Free Personalized Archetype Reading'),
		h.div('.seperator', ''),
		h.h2('Enter Your Details To Instantly Identify Your Personality Quirks, Innate Talents, And Hidden Weaknesses')
	])
]);
