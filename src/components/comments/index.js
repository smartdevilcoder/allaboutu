import { Component } from 'preact';
import hs from 'preact-hyperstyler';
import FacebookProvider, { Comments } from 'react-facebook';
import config from '../../../config';
import commentLogo from './assets/comment-logo.png';
import styles from './style.styl';

const h = hs(styles);

export default h(class comments extends Component {
  render() {
    return (!window.isDev) && h.div('.outer', {
      ref: ref => this.ref = ref,
    }, [h.div('.container', [
      // h.img({ src: commentLogo }),
      h.div('.heading', 'What archetype did you get? Let us know in the comments below!'),
      h(FacebookProvider, {
        appId: config.facebook.appId
      }, [h(Comments, {
        // href: 'http://individualogist.com/whats-your-archetype/',
        // href: 'individualogist.com/whats-your-archetype/',
        // href: 'individualogist.com',
        href: 'archetype.individualogist.com',
        // href: 'staging.archetype.individualogist.com',
        // href: '/comments',
        // appId: config.facebook.appId,
        // <div class="fb-comments" data-href="" data-numposts="5"></div>
        // href: 'https://developers.facebook.com/docs/plugins/comments#configurator',
        numPosts: 5,
        // width: '100%',
        orderBy: 'time',
      })]),
    ])]);
  }
});
