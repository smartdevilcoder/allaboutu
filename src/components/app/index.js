import { Component } from 'preact';
import hs from 'preact-hyperstyler';
import Router, { route } from 'preact-router';
import Case from 'case';
import store from '../../store';
import component from '..';
import archetypes from '../../data/archetypes';
import fixSubdomain from '../../utils/fix-subdomain';
import styles from './style.styl';

const h = hs(styles);

export default class App extends Component {
	constructor (props) {
		super(props);
		this.redirect = this.redirect.bind(this);
	}

	componentWillMount() {
		if ('new' in this.props.urlQuery) {
			store.clear();
			this.setState({
				urlQuery: this.props.urlQuery,
			});
		} else {
			this.setState(this.props);
		}

		if (this.props.urlQuery.name) {
			this.setState({
				formData: Object.assign({
					name: this.props.urlQuery.name,
				}, this.state && this.state.formData),
			});
			store.save(this.state);
		}

		if (this.props.urlQuery.email) {
			this.setState({
				formData: Object.assign({
					email: this.props.urlQuery.email,
				}, this.state && this.state.formData),
			});
			store.save(this.state);
		}

		if (this.props.urlQuery.archetype) {
			this.setState({
				quizData: Object.assign({
					archetype: this.props.urlQuery.archetype,
				}, this.state && this.state.quizData),
			});
			store.save(this.state);
		}

		if ('test' in this.props.urlQuery) {
			this.setState({
				formData: Object.assign({
					name: 'Testname',
					email: 'test@test.com',
				}, this.state && this.state.formData),
				quizData: Object.assign({
					archetype: 'caregiver'
				}, this.state && this.state.quizData),
			});
			store.save(this.state);
		}

		if (this.state.quizData && this.state.quizData.archetype && !archetypes[this.state.quizData.archetype]) {
			console.log(`Invalid archetype: '${this.state.quizData.archetype}'`);
			console.debug(this.state);
			this.setState({
				quizData: {},
			});
		}

		if (window.isDev) {
			console.log('this.state:', this.state);
		}
	}

	redirect(currentRoute = location.pathname) {
		console.log(`Redirect requested from`, currentRoute);
		// console.log(this.state.quizData);
		if (typeof currentRoute !== 'string') {
			console.log(`currentRoute:`, currentRoute);
			console.log(`this.path:`, this.path);
			console.log(`arguments:`, arguments);
			throw new Error('currentRoute is not a string');
		}
		const redirectFrom = from => {
			let to = from;
			if (this.state.quizData && this.state.quizData.archetype) {
				if (this.state.formData && this.state.formData.name && this.state.formData.email) {
					if (!['/reading', '/deluxe'].includes(from)) {
						to = '/reading';
					}
				} else {
					to = '/intro';
				}
			} else {
				to = '/quiz';
			}
			return from !== to && to;
		};

		const redirectTo = redirectFrom(currentRoute);
		if (redirectTo) {
			const msg = `redirecting from '${currentRoute}' to '${redirectTo}'`;
			console.log(msg);
			route(redirectTo);
			return msg;
		}
	}

	componentDidMount() {
		this.redirect();
		/*
		const redirecting = this.redirect();

		if (redirecting) {
			return redirecting;
		}	else {
			return null;
		}
		*/
	}

	updateGoogleTag(data) {
		if (typeof dataLayer !== 'undefined') {
			clearTimeout(this.updateGoogleTagTimeout);
			this.updateGoogleTagTimeout = setTimeout(() => dataLayer.push(data), 100);
		} else {
			console.log(`updateGoogleTag - doesn't exist`);
		}
	}

	render() {
		console.log('app render this.state', this.state);

		const paths = {};

		paths.quiz = () => h.div([h(component('quiz'), Object.assign({}, this.state, {
			onFinish: quizData => {
				this.setState({ quizData });
				store.save(this.state);
				route('/intro');
			}
		}, { redirect: this.redirect })), component('comments'), component('footer')]);

		paths.intro = () => h.div([h(component('intro'), Object.assign({ redirect: this.redirect }, this.state, {
			form: h(component('form'), Object.assign({}, this.state, {
				onSubmit: (formData) => {
					const newState = Object.assign({ formData }, this.state);
					store.save(newState);
					this.setState(newState);
					this.redirect('/reading');
				},
				componentDidMount: formEl => {
					// formEl.querySelector('input[type=hidden]').focus();
					window.scrollTo(0, 0);
				},
			}))
		})), component('comments'), component('footer')]);

		paths.reading = () => h.div([
			h(component('reading'), Object.assign({}, this.state, { redirect: ::this.redirect })),
			component('comments'),
			component('footer'),
		]);

		paths.deluxe = () => h.div([
			h(component('reading-deluxe'), Object.assign({}, this.props.urlQuery, this.state, { redirect: ::this.redirect })),
			component('comments'),
			component('footer'),
		]);

		const router = ({ path }) => {
			console.log(`Path requested: '${path}'`);
			this.updateGoogleTag({
				event: 'route-change',
				path,
				data: this.state,
			});
			if (path in paths) {
				document.title = Case.title(path) + ' | ' + window.originalTitle;
				return h(paths[path]);
			} else if (window.isDev) {
				return `(dev mode) Not redirecting to '/${path}'`;
			} else {
				path = fixSubdomain(path);
				location.assign(path);
				return `Redirecting to '${path}'...`;
			}
		};

		return h.div('.app', [h(Router, [h(router, { path: '/:path' })])]);
	}
}
