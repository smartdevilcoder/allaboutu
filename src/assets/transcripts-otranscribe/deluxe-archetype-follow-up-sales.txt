1

00:00 

If you’re watching this video,

{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/{{archetype}}.png', fadeIn: true} {{/fn}}

2

 00:02 

it means that you have already received your free archetype reading,

3

00:05 

and your free archetype report.

4

 00:08 

And welcome to the Individualogist family!

{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/{{archetype}}.png', fadeOut: true} {{/fn}}

5

00:11 

I want you to know that there is so much more

6

00:13 

that I can tell you about your archetype.

7

00:16 

This free reading is merely scraping the surface.

8

00:19 

I haven’t told you about your Shadow, your Anima/Animus, your Ego, and your life path.

9

00:26 

Your Shadow is the dark side of your personality

10

00:29 

where all of your suppressed thoughts and desires are stored.

11

00:33 

Your Anima and Animus represents the imbalance

12

00:36 

between masculinity and femininity in your character.

13

00:40 

Your Ego is the centre of your consciousness

14

00:43 

that needs to be controlled in order to find true joy.

15

00:46 

The Deluxe Archetype Report tells you everything

16

00:50 

that you need to know about all of that, and more.

17

00:53 

It tells you about…

18

00:55 

The choices that you’ve made and why you made them

19

00:58 

Your ideal life partner or companion

20

01:01 

Your path towards unlocking enormous amounts of success

21

01:06 

The challenges that lie ahead of you

22

01:09 

Your path towards finding fulfilment in this lifetime

23

01:13 

The Deluxe Archetype Report is a reading that’s far more detailed, and far more intimate.

24

01:18 

If you thought that this free archetype report was accurate,

25

01:22 

you’re about to have your mind blown.

26

01:24 

In this reading, you will discover who you are. Yes, who you REALLY are.

27

01:30 

It’s going to be a completely raw and uncensored look into your personality.

28

01:35 

All of this information will be presented to you in excruciating detail.

29

01:40 

If you’re like most people, you’ll find that it’s an extremely emotional read.

30

01:45 

Imagine the most emotional film you’ve ever seen, and multiply that emotion by ten times.

31

01:52 

You’ll find yourself identifying with every single characteristic of your personality.

32

01:57 

These are things that not even your closest friends or family know about you.

33

02:02 

You might not even have realized some of these things yourself.

34

02:07 

You will get emotional when you discover the reason for the

35

02:10 

trials and misfortunes that you’ve been through.

36

02:13 

You will confront the struggles that you’ve been in constant battle with.

37

02:17 

You will get emotional when you learn about the amazing, wonderful gifts that life has in store for you.

38

02:24 

You will pick up, examine, and embrace every single piece of your character, personality, and entire being.

39

02:32 

You will place everything back together and witness the beauty that it all forms.

40

02:37 

You will master your weaknesses and learn how to improve them.

41

02:41 

You will learn about your strengths and discover how to leverage on them.

42

02:46 

At the end of this detailed report,

43

02:48 

you’ll take everything that you’ve experienced and take advantage of them.

44

02:52 

The answers to all of your life’s questions, to all of your life’s complications,

45

02:58 

to all of your life’s encounters, failures, and successes,

46

03:02 

are waiting for you in your Deluxe Archetype Report.

47

03:05 

If you’re unsure of the path that you’re currently on,

48

03:09 

this report will tell you everything you need to know about where to go NEXT.

49

03:14 

If you’re wondering whether your actions are right or wrong,

50

03:17 

this report will grant you unquestionable direction.

51

03:21 

If the people around you are holding you back, this report will give you all the clarity you need.

52

03:26 

Everything I’ve just talked about is just waiting for you.

53

03:30 

And the only thing standing between you and the life you’ve always dreamed of is YOURSELF.

54

03:36 

Tell me, do you want to be so successful that you won’t have to worry about money any more?

55

03:41 

Do you want to make the most meaningful relationships?

56

03:44 

Do you want to have good fortune in everything that you do?

57

03:48 

Most importantly, do you want to live the life that you’ve always dreamed of?

58

03:53 

The Deluxe Archetype Report WILL give you all of that on a silver platter.

59

03:59 

Unfortunately, I can’t lead you through this journey if you don’t let me.

60

04:03 

You need to take that first step.

61

04:05 

You need to trust your instincts and take that leap of faith…

62

04:09 

If you’re worrying about your career, your relationships, your health, your finances…

63

04:14 

If you haven’t the slightest clue of where you’re going to be 10, 5, 3 or even 1 year from now…

64

04:21 

If you want to become a better version of yourself…

65

04:25 

If you want to take back the reigns and choose the direction of your life…

66

04:29 

All of the answers are waiting for you in your Deluxe Archetype Report.

67

04:34 

To make things a little easier for you,

68

04:36 

I’ve simplified everything for you to make the right choice.

69

04:40 

Here’s what the Deluxe Archetype Report will almost instantly accomplish for you:

70

04:45 

You’ll unlock the deepest desires of your heart and soul; desires

71

04:50 

that you would never expect, and exactly how you can go about achieving them.

72

04:54 

You’ll realize opportunities before they’ve even presented themselves.

73

04:59 

You’ll recognize potential pitfalls from miles away and

74

05:03 

learn how you can protect yourself or even avoid them completely.

75

05:06 

You’ll be spoon-fed the necessary knowledge and tools to achieve what you truly want.

76

05:12 

You’ll achieve the life that you’ve always wanted;

77

05:15 

the life that you’ve wanted for the longest time.

78

05:19 

And finally, you’ll learn how you can make the absolute best of it!

79

05:23 

But that’s not even the half of it.

80

05:25 

In just a minute,

81

05:27 

I’m going to share with you even more amazing ways that will help you achieve the life that you truly want.

82

05:33 

Before we get into that,

83

05:35 

Allow me to share this important secret with you.

84

05:38 

Now, pay close attention.

85

05:40 

Individuation is based on thousands of years of in-depth analysis and research.

86

05:46 

Famous philosophers and psychologists have improved and enhanced the principles of individuation for that long.

87

05:54 

You need specifics?

88

05:56 

Individuation has been present during the Renaissance,

89

05:58 

during the Medieval era, and dates all the way back to 350BC (Aristotle).

90

06:04 

And because of the technology that we have access to today,

91

06:08 

we can achieve the same success that TENS OF THOUSANDS of ordinary people have experienced with individuation.

92

06:15 

The best part?

93

06:17 

You can uncover these hidden secrets with a simple click of a button.

94

06:21 

I’ve dedicated more than 10 years of my life towards researching and studying individuation.

95

06:27 

And I can guarantee that’s what I’ll be doing for the rest of my life.

96

06:32 

That’s a fact.

97

06:33 

And here’s something else that’s going to blow your mind…

98

06:37 

Are you ready for this?

99

06:38 

The Deluxe Archetype Report is EASILY the most DETAILED, ACCURATE, and REVEALING individuation report in the ENTIRE WORLD.

100

06:48 

We have dedicated decades, including 10 years from myself,

101

06:52 

to creating a report that’s as detailed and accurate as the Deluxe Archetype Report.

102

06:58 

Every single word has been handwritten, edited, and re-edited,

103

07:03 

to ensure that this report contains ZERO FLUFF

104

07:06 

and only the MOST BENEFICIAL information about you.

105

07:10 

Years of thought have gone into every single sentence.

106

07:14 

Years of constant editing and corrections have gone into perfecting each paragraph.

107

07:20 

Years of research have gone into the contents of this report.

108

07:25 

It’s been completely personalized for you, and only you, {{name}}.

109

07:30 

No one else in the world can pick it up and benefit from it in the same way that you will.

110

07:35 

Embarking on this journey WILL open doors for you

111

07:39 

that you would have never discovered without taking this first step.

112

07:43 

It’s an eye-opening experience like no other.

113

07:46 

The contents of this report can never be found anywhere else.

114

07:50 

It hasn’t been published or even copied. NOT ONCE.

115

07:55 

{{name}}, this is the one and only place you’ll be able to get your hands on it.

116

07:59 

Everything that you read from this report is completely tailored to your main archetype.

117

08:05 

It will give you the EXACT system that over 70,000 ordinary people,

118

08:10 

including myself, have used to completely CHANGE OUR LIVES.

119

08:15 

It’s entirely unique to you.

120

08:17 

And that’s what individuation is all about – discovering your true self.

121

08:23 

I have no doubt in my mind that you will be absolutely stunned

122

08:26 

as you read about your past, your present, and your future.

123

08:30 

And at the end of it all, you will be RAVING about its degree of ACCURACY.

124

08:36 

But more importantly, you will experience significant improvements in your daily life

125

08:41 

after you apply the techniques and the knowledge that you learn from this report.

126

08:45 

Personally, I get emails every single day from people sharing their successes after receiving their Deluxe Archetype Report from me.

127

08:54 

But I’m not done just yet. I’m extremely passionate about individuation.

128

09:00 

And that’s precisely why I want you to experience the real value of individuation.

129

09:06 

To ensure that you gain the maximum value from this report,

130

09:10 

I’ll be sending you THREE additional FREE reports along with it…

131

09:15 

All of this on top of your Deluxe Archetype Report.

132

09:19 

That’s how much I believe in this product.

133

09:21 

Like your Deluxe Archetype Report, these additional reports are easy to understand and extremely valuable.

134

09:29 

{{name}}, You will receive:

{{#fn}} {fn: 'sliderImageSingle', path:'images/deluxe/deluxe-books-flat-additional.png', fadeIn: true} {{/fn}}

135

09:31 

The Exploring Your Birthdate with The Chinese Zodiac eBook,

136

09:35 

The Discovering Your Aura eBook,

137

09:37 

and The Beginners Guide to the Fengshui Paradigm eBook

138

09:41 

I’ll be completely honest with you.

139

09:43 

The total value of all of these products is at least $244.00,

140

09:48 

and that’s practically pennies compared to how much value you’ll receive from them!

{{#fn}} {fn: 'sliderImageSingle', path:'images/deluxe/deluxe-books-flat-additional.png', fadeOut: true} {{/fn}}

141

09:53 

After all, you can’t put a dollar figure on improving your life –

142

09:57 

because such a gift would be practically priceless.

143

10:01 

Money can always be earned, and there’s only so much time we have left to live.

144

10:06 

But no {{name}}, I’m NOT going to ask you for that amount of money.

145

10:11 

I’m not even going to ask you for $200.

146

10:14 

Well, what about $100? Nope…

147

10:17 

Well, but what about $50?

148

10:19 

That seems like a pretty fair price.

149

10:21 

But no, I’m not even going to ask you for that much.

150

10:25 

So, here’s what we’re going to do.

151

10:27 

I’m going to cut you an exclusive, one-time deal.

{{#fn}} {fn: 'sliderImageSingle', path:'images/deluxe/deluxe-books-flat-additional.png', fadeIn: true} {{/fn}}

152

10:30 

You’ll get the full package, including the Deluxe Archetype Report, for only $37.

153

10:37 

That’s it.

154

10:38 

But I want you to share with me your success story.

155

10:41 

I read every single one of my emails and respond to them personally,

156

10:46 

and I want to read about your experiences too.

157

10:49 

We need the feedback to keep on improving our products,

158

10:53 

and it definitely encourages us to keep doing what we do.

159

10:57 

The Deluxe Archetype Report is all yours for just $37.

{{#fn}} {fn: 'sliderImageSingle', path:'images/deluxe/deluxe-books-flat-additional.png', fadeOut: true} {{/fn}}

160

11:02 

Now, you have two options.

161

11:04 

You can continue on the life path that you’re on right now

162

11:07 

and stay exactly the way that you are, unaware of your life’s purpose,

163

11:12 

your calling, and your true personality.

164

11:14 

Or, you can have your life’s purpose, your calling, your true personality, and your life’s direction revealed to you

165

11:23 

AND experience an abundance of love, health, and wealth in your life.

166

11:28 

All you have to do is click the button below and your Deluxe Archetype Report,

167

11:32 

and all 3 FREE bonus reports, will be sent to you INSTANTLY.

168

11:38 

But that’s not the best that I can do. I know I can do better than that.

169

11:43 

On top of giving away the whole package for just $37,

170

11:47 

I’m also going to make the following guarantee for your own personal protection:

{{#fn}} {fn: 'sliderImageSingle', path:'images/pop-up/60-day-money-back-guarantee.png', fadeIn: true} {{/fn}}

171

11:51 

{{name}},take your time to look through your Deluxe Archetype Report and everything else that comes with it.

172

11:57 

If you decide within the next 60 days that you’re not completely satisfied with your Deluxe Archetype Report,

173

12:04 

Just drop us an e-mail at individualogist.com and we’ll issue you a full refund.

174

12:10 

No questions and no explanations will be necessary.

175

12:14 

I’m making this guarantee

176

12:16 

Because I’m 100% certain that this report has the capacity to truly turn your life around.

177

12:23 

That’s how much I believe in the process of individuation,

178

12:27 

and that’s how much I believe you will benefit from it.

{{#fn}} {fn: 'sliderImageSingle', path:'images/pop-up/60-day-money-back-guarantee.png', fadeOut: true} {{/fn}}

179

12:30 

{{name}}, so, no matter what, you’ve got the longer end of the stick.

180

12:35 

There is absolutely no risk involved, and that’s all up to you

181

12:39 

and whether you decide to take this life-changing path.

182

12:42 

When you click on the button shortly, you’ll be taken to a 100% secure order page.

183

12:48 

After filling out a few questions,

184

12:50 

you’ll have the Deluxe Archetype Report and the rest of the reports sent directly to you instantly.

185

12:56 

You won’t have to wait a second longer to begin your individuation journey.

186

13:01 

There’s not much time left, because this unique offer is only going to be extended to you just once and, only once.

187

13:09 

To receive your Deluxe Archetype Report, and the 3 additional bonuses, at the once in a life time price of only $37,

188

13:17 

You need to act right now.

189

13:20 

Just click the button below to experience a real change in your life.

190

13:25 

I’m offering it to you right now for just $37, but {{name}}, you need to make the decision NOW.

191

13:31 

People approach me constantly with concerns about their careers, and I’ve used the same information

192

13:37 

and techniques in this package to help them SKYROCKET their SUCCESS by TEN FOLDS.

193

13:43 

Married couples on the verge of separation have approached me for the secrets of individuation as well,

194

13:49 

and guess what?

195

13:50 

They’re now enjoying the most rewarding relationships.

196

13:54 

All of this is made possible because of this EXCLUSIVE Deluxe Archetype Report Package.

197

14:00 

I can go on for days about all of the success stories that

198

14:03 

others have experienced after receiving the Deluxe Archetype Report.

199

14:07 

But the truth is, I honestly want you, {{name}}, to be one of the success stories that I share with people.

200

14:14 

When I embarked on this journey, there was no one around to help me.

201

14:19 

In fact, we didn’t even have the Internet back then.

202

14:22 

The only thing I could do was sit in that library for hours and hours every single day,

203

14:28 

finding every book that I could about individuation.

204

14:31 

I read every page and every word.

205

14:36 

To date, I’ve probably gone through over hundreds of thousands of pages and thousands of books and materials on individuation.

206

14:43 

I’m that passionate about it, and that’s why I want to share this gift with you now.

207

14:49 

Applying individuation is what truly saved me.

208

14:53 

It’s what turned my life around, and it’s what helped me to find joy.

209

14:59 

Not many of us are lucky enough to have experienced true joy, but that’s what I’m going to help you find.

210

15:05 

Today, you’ve stumbled upon the rare opportunity.

211

15:08 

The opportunity for you to completely change your life.

212

15:12 

This might be the one and only time that life hands you a reset button.

213

15:17 

This is your chance to discover your true self and unlock a massive amount of benefits

214

15:23 

that will change your life for the better.

215

15:25 

I’m more than happy to make this one-time offer to you, but just like you,

216

15:30 

I only have 24 hours in a day.

217

15:32 

If you don’t respond now, I’ll have no choice but to invest my time in people

218

15:37 

who have already taken advantage of this once in a lifetime opportunity.

219

15:42 

And sadly enough, your opportunity would have been lost.

220

15:46 

Not many people in the world will have the opportunity that you have right now {{name}}.

221

15:51 

This is the opportunity where your life can REALLY turn around.

222

15:56 

{{name}}, this is your time to shine and make the decision

223

15:59 

that will align your direction with your destiny.

224

16:02 

The thing is, you could probably find a book about Individuation from a local book store,

225

16:08 

which will contain a chapter or a couple of pages about your archetype.

226

16:12 

But that single chapter or couple of pages isn’t going to be very helpful. So, here’s the truth.

227

16:19 

{{name}}, The Deluxe Archetype Report is an excruciatingly detailed report about your archetype,

228

16:25 

containing 200 highly informative pages of actionable and thought-provoking strategies and methodologies.

229

16:33 

Once again, all of the information is customized to your specific archetype.

230

16:39 

It breaks down an in-depth outlook of your life journey and all of its obstacles.

231

16:45 

There is no other product that comes as close in terms of detail and information.

232

16:51 

The Deluxe Archetype Report is 100% owned and authored by us. {{name}},

233

16:56 

this is the only place where you’re able to get it, or anything like it.

234

17:01 

And if you’ve tried similar products that didn’t help before, here’s the difference.

235

17:06 

{{name}}, other products assume that everyone functions and processes information in the same way.

236

17:11 

The law of attraction assumes that everyone is built with the same framework.

237

17:16 

Manifestation programs assume that everyone will respond the same way.

238

17:21 

Meditation methods assume that everyone experiences calmness similarly.

239

17:27 

None of that can be true, because, we are all different individuals.

240

17:32 

We have different fears, different circumstances, and different life experiences.

241

17:37 

Individuation and archetypes take that into account.

242

17:41 

Our readings are specifically catered to your archetype to provide you

243

17:46 

with actionable steps that you can take towards making a true change in your life.

244

17:51 

It reveals callings and directions that are specific to you.

245

17:55 

That’s why it’s going to work.

246

17:58 

This product seeks to bring the term “personal” back into personal development;

247

18:03 

an industry littered with impersonal products

248

18:06 

that have strayed away from what personal development is meant to be.

249

18:10 

But beyond that, the Deluxe Archetype Report is proven and supported by science.

250

18:16 

Individuation is a psychological process that was created by thought leaders in the psychological world decades ago.

251

18:24 

In fact, principles of Individuation can be traced all the way back to the days of Aristotle.

252

18:30 

This means that Individuation has stood the test of time to be a proven personal development process.

253

18:38 

There have also been numerous conclusive scientific studies about analytical psychology and individuation across the globe.

{{#fn}} {fn: 'sliderImageSingle', path:'images/deluxe/deluxe-books-flat-additional.png', fadeIn: true} {{/fn}}

254

18:46 

Unlock your Deluxe Archetype Report and the full package of 3 FREE reports now,

255

18:52 

before they’re no longer available to you!

256

18:55 

All you have to do is click the button below {{name}},

257

18:57 

and you’ll receive the entire Deluxe Archetype Report package immediately.

258

19:02 

The time has come for you to take action. Now, let’s get this show on the road.

259

19:07 

We’ll speak again soon, and that’s a promise.