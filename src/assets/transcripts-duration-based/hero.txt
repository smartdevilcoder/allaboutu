2.000: Hello {{name}},
2.000: If you’ve made it here, then congratulations!
3.000: Because this means that your archetype is the Hero!{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/hero.png', fadeIn: true} {{/fn}}
4.000: But what does this actually mean?
4.000: Don’t worry, everything you need to know will be on this page.
4.000: Stick to the end of it and I promise that you’ll…
2.000: Experience something truly magical.{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/hero.png', fadeOut: true} {{/fn}}
3.000: The quiz that you’ve just taken is designed to identify your archetype…
5.000: A personality profile that’s broken down your entire being.
5.000: Your tendencies, your strengths, your past, and your future.
5.000: Your archetype takes into account your gender, your marital status…
5.000: How you answered the quiz, and how long you took to get those answers.
5.000: That’s right, {{name}}.
1.000: You can use that information to make astronomical changes in your life.
5.000: You’ll be able to leverage on your strengths, improve on your weaknesses...
6.000: And embark on the life that you’ve always dreamed of.
4.000: But before we get into that, I want to share with you about your archetype…
5.000: The Hero, and what it means.
3.000: {{name}}, you truly are a unique individual.
4.000: And I genuinely believe that this world needs more people like you.
4.000: You are a brave and deeply intelligent being…
5.000: Not necessarily in the sense of academics…
3.000: But in terms of connecting with people…
2.000: And building rapport with people from all sorts of backgrounds.
4.000: You’re different that way, {{name}}.
3.000: While the world walks in one direction…
3.000: You have no qualms with going against it.
3.000: That’s what makes you so special, {{name}}.
4.000: You are the epitome of selflessness.
2.000: You’re able to empathize with anyone and everyone.
4.000: And if given the opportunity to lay down yourself for…
4.000: The sake of someone else…
2.000: You have the courage to do so without hesitation.
3.000: That’s because as a Hero, you are highly compassionate.
5.000: You put the needs of others before your own…
4.000: And you’re able to connect with strangers…
2.000: Just as well as you’re able to connect with your closest loved ones.
5.000: {{name}}, you might not know it yet…
2.000: But you are more morally and ethically upright than others.
4.000: You hold your values and principles close to your heart…
3.000: And you’re always ready to stick to your guns.
3.000: There is something else about you, {{name}}…{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeIn: true} {{/fn}}
3.000: You are not just an everyday “Hero”.
4.000: You are the Hero who will one day save the world.
4.000: Not like a superhero…
2.000: But you will create solutions to the world’s biggest problems.{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeOut: true} {{/fn}}
4.000: {{name}}, I’m getting the sense that you’re going to live a long, and happy life.
5.000: And although success might not be at your feet right now…
4.000: Your riches and fulfilment are merely waiting for you.
4.000: {{#fn}} {fn: 'confirmToContinue', type: 'ok', message: 'Would you like to learn more about your path to fulfilment?', button: 'Click here to continue'} {{/fn}}.
3.000: Even though you will triumph, your path to success will not come easy.
5.000: Talking to people is never a problem for you…
3.000: But I’m getting the sense that you tend to struggle when it comes to being yourself.
5.000: Your nature revolves so much around how people perceive you…
5.000: That you can become too cautious of how you act.
4.000: {{name}}, don’t let that discourage you.
2.000: The world needs your selfless and kind nature.
4.000: The world needs people like you to be in positions of power.
4.000: You are what keeps the world grounded.
3.000: You do, however, need to understand that it’s important for you…
5.000: To be who you are without worrying about the perceptions of others.
4.000: That way, you’ll be able to build trust and create…
4.000: Even more fruitful interactions.
3.000: After all, {{name}}, your confidence is unparalleled.
4.000: And there is no better way to exercise confidence than just simply being you.
6.000: {{name}}, I can already tell that your friends and family hold high regards for you.
6.000: You’re the one who they come to for detailed advice…
4.000: A listening ear, or just for a quick chat.
4.000: {{name}}, you might not know it…
2.000: But you truly are a hero to more people than you think.
3.000: {{name}}, I’m sensing that you’re the type of person who has many friends.
6.000: You’re well-liked by most people…
3.000: And you’re not someone who has trouble with making a good impression.
4.000: In fact, you have no trouble at all when it comes to…
3.000: Impressing others with your intelligence and courageous nature.
4.000: But you’re extremely selective with who you choose to let into your life.
5.000: You prefer to keep your loved ones close, and your wolf pack small.
5.000: {{name}}, you are incredibly grounded.
3.000: You possess the unique ability to discern right from wrong…
4.000: And you’re never afraid to put your foot down.
3.000: You have a great deal to offer to this world.
3.000: {{name}}, you are a natural-born leader.
3.000: I sense that you will find success and fulfilment as…
5.000: An entrepreneur, a politician, a police officer, or a doctor.
6.000: You can be very protective, {{name}}.
2.000: Especially as a parent.
2.000: You love your children, friends, and family fiercely.
5.000: You are willing to give and sacrifice everything that you have for them.
4.000: {{name}}, you are going to make a lasting mark in your lifetime.
5.000: You’re able to look at the world from a detailed and compassionate perspective.
4.000: You recognize the everyday problems of the world…
4.000: And you have what it takes to make the world a better and safer place for everyone.
6.000: {{name}}, you are practically the definition of “bravado”.
5.000: You’re never afraid to rise to challenges of all sorts.
5.000: But beyond that, you can be extremely innovative when it comes to solving problems.
6.000: You excel at one-on-one interactions…
3.000: And never fail to make an excellent impression on others.
4.000: Your charisma and eloquence intrigues people.
3.000: Ultimately, {{name}}, you are a realist.
5.000: You have a clear conscience.
2.000: Discerning right from wrong has never been a difficult task for you.
5.000: I’m getting the sense that you’re not too concerned about monetary riches…
5.000: Although you do wish you had more wealth.
3.000: There is, however, a greater purpose for you in this lifetime, {{name}}.
6.000: However, {{name}}, you will have to keep pushing your limits…
4.000: Before you eventually discover your life’s purpose and direction.
4.000: And that is something that you need to work towards.
3.000: At times, it might feel as if you’re living your life for others.
6.000: That’s not what you want, {{name}}.
2.000: You want to live life for yourself.
3.000: You want to love yourself.
2.000: When you’re able to do that, success and the life of your dreams will follow.
5.000: And that’s what Individuation aims to teach you.
4.000: But there’s more to what I’ve learned about you, {{name}}…
4.000: It feels to me that you’re a deeply emotional and spiritual person.{{#fn}} {fn: 'sliderImageSpiritual', fadeIn: true} {{/fn}}
5.000: You’re connected to your soul in a way that’s far more profound than others.
5.000: Your thought process does not follow the rules and logic of the world.
5.000: In fact, I get the sense that you have quite the imagination.
4.000: Your thoughts and dreams are vivid, colourful…
3.000: And symbolic of who you are as an individual.{{#fn}} {fn: 'sliderImageSpiritual', fadeOut: true} {{/fn}}
4.000: And unlike others, you’re never one to shy away from a challenge or a task…
5.000: No matter how difficult it appears to be.
3.000: {{name}}, you’re not someone who likes to argue.
4.000: You tend to avoid conflict and you’re uncanny…
2.000: When it comes to devising win-win situations.
4.000: But when backed into a corner, you’re not one to back down.
4.000: You fight fiercely for your loved ones and for righteousness.
4.000: You’re the first person to step in when you see a fight breaking out.
5.000: You’re the first person to reach for your pockets in the sight of a struggling busker.
5.000: You’re the first person your friends call when they’ve had a horrible day.
5.000: You’re the one who people rely on and look up to.
4.000: {{name}}, you are the hero who will one day save the world.
4.000: But every superhero needs a side-kick…
4.000: A partner in crime who will stick by them when the going gets tough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeIn: true} {{/fn}}
5.000: This little chart I’ve prepared represents the…
3.000: Archetypes that you’re most likely to end up with.
3.000: Or if you’re already in a committed relationship…
3.000: There’s a really good chance that your spouse is one of those archetypes.
4.000: {{name}}, you’re not the kind of person that has trouble when it comes to love.
5.000: Love interests find themselves drawn to your strength and decisiveness.
6.000: You’re sort of a rare breed when it comes to love.
3.000: You communicate better than everyone else…
2.000: And you’re a thoughtful individual.
3.000: The relationships that you forge with potential lovers are always profound and deep.
6.000: You become very selective with…
2.000: Who you choose to date and who you choose to end up with…
4.000: For good reason, of course.
2.000: I sense that in the coming years…
3.000: You will be the happiest you’ve ever been in that regard.
4.000: {{name}}, I’m sensing that your love life isn’t going to be the smoothest…
5.000: But you will find that happiness that you’re looking for soon enough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeOut: true} {{/fn}}
5.000: But don’t forget, {{name}}…
1.000: How you perceive yourself should not be determined by your love life.
5.000: Your spouse or partner is meant to play a supporting role in your life…
5.000: Not the lead.
1.000: Focus on your own personal growth, and I promise you, {{name}}…
4.000: You will soon discover heavenly rewards.