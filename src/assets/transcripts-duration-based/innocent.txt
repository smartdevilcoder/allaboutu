2.000: Hello {{name}},
2.000: If you’ve made it here, then congratulations!
3.000: Because this means that your archetype is the Innocent!{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/innocent.png', fadeIn: true} {{/fn}}
4.000: But what does this actually mean?
2.000: Don’t worry, everything you need to know will be on this page.
4.000: Stick to the end of it and I promise that you’ll experience something truly magical.{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/innocent.png', fadeOut: true} {{/fn}}
6.000: The quiz that you’ve just taken is designed to identify your archetype…
5.000: A personality profile that’s broken down your entire being.
4.000: Your tendencies, your strengths, your past, and your future.
5.000: Your archetype takes into account your gender, your marital status…
5.000: How you answered the quiz, and how long you took to get those answers.
5.000: That’s right, {{name}}.
2.000: You can use that information to make astronomical changes in your life.
5.000: You’ll be able to leverage on your strengths, improve on your weaknesses…
4.000: And embark on the life you’ve always dreamed of.
5.000: But before we get into that, I want to share with you about your archetype…
5.000: The Innocent, and what it means.
3.000: {{name}}, you are truly a unique individual.
4.000: And I genuinely believe that this world needs more people like you.
5.000: You are an extremely positive and optimistic being.
3.000: You are capable of finding joy in the smallest of things, and you always have the purest of intentions.
7.000: You’re different that way, {{name}}.
2.000: While the world walks in one direction, you have no qualms with going against it.
6.000: That’s what makes you so special, {{name}}.
4.000: You’re deeply sensitive.
2.000: You’re able to empathize with anyone and everyone.
4.000: That’s because as an Innocent, you are highly compassionate.
4.000: You put the needs of others before your own…
3.000: And you’re able to connect with strangers just as well as you’re able to connect with your closest loved ones.
7.000: But beyond that, you are also an incredibly strong and dedicated individual.
6.000: {{name}}, you might not know it yet…
3.000: But you are more morally and ethically upright than others.
4.000: You hold your values and principles close to your heart, and you’re always ready to stick to your guns.
7.000: There is something else about you, {{name}}…{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeIn: true} {{/fn}}
3.000: You represent what it means to be strong.{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeOut: true} {{/fn}}
4.000: {{name}}, I’m getting the sense that you’re going to live a long, and abundant life.
6.000: And although success might not be at your feet right now, your riches and fulfilment are merely waiting for you.
7.000: {{#fn}} {fn: 'confirmToContinue', type: 'ok', message: 'Would you like to learn more about your path to fulfilment?', button: 'Click here to continue'} {{/fn}}.
2.000: Even though you will triumph, your path to success will not come easy.
5.000: However, I have no doubt that you will effortlessly power through the darkest of days.
6.000: There is no one else in the world who’s able to match your spiritual and mental strength.
5.000: Your faith is truly unbreakable.
6.000: {{name}}, don’t let the judgments of the world discourage you.
5.000: The world needs your selflessness and strong nature.
4.000: The world needs people like you to be in positions of power.
4.000: You play a fundamental role in the world, representing the importance of self-belief and perseverance.
6.000: You do, however, need to understand that it’s easy for you to stay within your comfort zone.
6.000: Instead, you should be constantly jumping into new opportunities and discovering your path as it unfolds.
8.000: That way, you’ll be able to continue to grow as a person, as an individual, and as an Innocent.
7.000: After all, {{name}}, your personal growth will ultimately determine your path to fulfilment.
7.000: Of course, I have no doubt that you will make it there.
4.000: {{name}}, I can already tell that your friends and family really look up to you.
6.000: You’re the one who they come to for detailed advice, a listening ear, or just a quick chat.
7.000: You’re not the type of person to have huge circles of friends.
4.000: You’re not someone who has trouble with making friends.
3.000: In fact, you have no trouble at all when it comes to impressing others with your kind and sweet nature.
9.000: You tend to be more accepting of others, allowing you to forge a great number of friendships.
6.000: Nevertheless, you prefer to keep your loved ones close, and your wolf pack small.
5.000: {{name}}, you are incredibly nurturing.
5.000: You possess the unique ability to connect with others and blend into any social group.
5.000: You have a great deal to offer to this world.
3.000: Friend, you are a natural-born leader.
3.000: I sense that you will find success and fulfilment as…
3.000: A nurse, a motivational speaker, a therapist, or a businessperson.
8.000: You can be very protective, {{name}}.
2.000: Especially as a parent.
3.000: You love your children, friends, and family fiercely.
4.000: You are willing to give and sacrifice everything that you have for them.
4.000: {{name}}, you are going to make a lasting mark in your lifetime.
5.000: You’re able to look at the world from a broad and altruistic perspective.
4.000: You see the importance of personal growth, and…
3.000: You have what it takes to make a massive amount of impact in the world.
4.000: {{name}}, you are on a quest – a quest for happiness.
6.000: I’m sensing that you’ve been struggling with finding fulfilment.
4.000: Please allow me to say this…
2.000: The light at the end of the tunnel is approaching…
3.000: And you will find your fulfilment a lot sooner than you expected.
4.000: Ultimately, {{name}}, you are a realist.
4.000: You have a clear conscience.
2.000: Discerning right from wrong has never been a difficult task for you.
4.000: I’m getting the sense that you’re not too concerned about monetary riches…
4.000: Although you do wish you had more wealth.
3.000: There is, however, a greater purpose for you in this lifetime, {{name}}.
5.000: However, {{name}}, I’m sensing that you’re afraid of failure.
6.000: But you should’t be afraid of anything at all.
3.000: Your positivity and strength form the foundation of success.
4.000: As long as you continue to act on new opportunities for personal growth…
4.000: You will find the success that you’re looking for.
4.000: At times, it might feel as if you’re living your life for others.
6.000: That’s not what you want, {{name}}.
2.000: You want to live life for yourself.
3.000: You want to love yourself.
2.000: When you’re able to do that, success and the life of your dreams will follow.
6.000: And that’s what Individuation aims to teach you.
4.000: There’s more to what I’ve learned about you, {{name}}…
5.000: It feels to me that you’re a deeply emotional and spiritual person.{{#fn}} {fn: 'sliderImageSpiritual', fadeIn: true} {{/fn}}
5.000: You’re connected to your soul in a way that’s far more profound than others.
5.000: Your thought process does not follow the rules and logic of the world.
4.000: In fact, I get the sense that you have quite the imagination.
6.000: Your thoughts and dreams are vivid, colourful, and…{{#fn}} {fn: 'sliderImageSpiritual', fadeOut: true} {{/fn}}
4.000: Symbolic of who you are as an individual.
4.000: {{name}}, you’re not someone who likes to argue.
3.000: You tend to avoid conflict and you’re uncanny when it comes to devising win-win situations.
7.000: But when backed into a corner, you’re not one to back down.
3.000: You fight fiercely for your loved ones and for righteousness.
4.000: You’re the first person to step in when you see a fight breaking out.
4.000: You’re the first person to reach for your pockets in the sight of a struggling busker.
4.000: You’re the first person your friends call when they’ve had a horrible day.
5.000: You’re the one who people rely on and look up to.
6.000: You might not know this, but the people around you draw inspiration…
5.000: …Motivation, and strength from your energy.
3.000: Your purity is unparalleled.
3.000: {{name}}, you are the angel overlooking the goodness of the world.
5.000: But even angels need someone to rely on…
4.000: A partner in crime who will stick by them when the going gets tough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeIn: true} {{/fn}}
4.000: This little chart I’ve prepared represents the archetypes that you’re most likely to end up with.
6.000: Or if you’re already in a committed relationship…
2.000: There’s a really good chance that your spouse is one of those archetypes.
5.000: {{name}}, you’re not the kind of person that has trouble when it comes to love.
4.000: People find themselves drawn to your charisma and kind nature.
5.000: You’re sort of a rare breed when it comes to love.
4.000: You communicate better than anyone else, and you’re an incredibly thoughtful individual.
5.000: The relationships that you forge with potential lovers are always profound and deep.
7.000: You become very selective when it comes to who you choose to date…
4.000: And who you choose to end up with.
2.000: For a good reason, of course.
2.000: I sense that in the coming years, you will be the happiest you’ve ever been in that regard.
6.000: {{name}}, I’m sensing that your love life isn’t going to be the smoothest…
5.000: But you will find that happiness that you’re looking for soon enough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeOut: true} {{/fn}}
4.000: But don’t forget, {{name}}…
2.000: How you perceive yourself should not be determined by your love life.
5.000: Your spouse or partner is meant to play a supporting role in your life…
4.000: Not the lead.
1.000: Focus on your own personal growth, and I promise you, {{name}}…
4.000: You will soon discover heavenly rewards.