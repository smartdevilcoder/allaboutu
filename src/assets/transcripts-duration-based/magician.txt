2.000: Hello {{name}}
2.000: If you’ve made it here, then congratulations!
3.000: Because this means that your archetype is the Magician!{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/magician.png', fadeIn: true} {{/fn}}
4.000: But what does this actually mean?
3.000: Don’t worry, everything you need to know will be on this page.
4.000: Stick to the end of it and I promise that you’ll experience something truly magical.{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/magician.png', fadeOut: true} {{/fn}}
6.000: The quiz that you’ve just taken is designed to identify your archetype…
5.000: A personality profile that’s broken down your entire being.
4.000: Your tendencies, your strengths, your past, and your future.
6.000: Your archetype takes into account your gender, your marital status…
5.000: How you answered the quiz, and how long you took to get those answers.
4.000: That’s right, {{name}}.
3.000: You can use that information to make astronomical changes in your life.
4.000: You’ll be able to leverage on your strengths, improve on your weaknesses…
5.000: And embark on the life that you’ve always dreamed of.
3.000: But before we get into that, I want to share with you about your archetype…
5.000: The Magician, and what it means.
2.000: {{name}}, you truly are a unique individual.
4.000: And I genuinely believe that this world needs more people like you.
4.000: You are an intellectual and deeply spiritual being…
4.000: Not necessarily in the sense of religion, but in terms of your understanding of the universe.
5.000: You’re different that way, {{name}}.
3.000: While the world walks in one direction, you have no qualms with going against it.
5.000: That’s what makes you so special, {{name}}.
4.000: You have a profound understanding when it comes to the inner-workings of human beings.
6.000: You’re able to empathize with anyone and everyone…
3.000: And you’re able to discern what people really want.
3.000: That’s because as a Magician…
2.000: You are able to recognize the most intricate details about people.
4.000: You are constantly on the search for knowledge that will help you improve as an individual.
5.000: {{name}}, you might not know it yet…
3.000: But you are an innately talented individual and an excellent all-rounder.
5.000: You are a capable individual who’s able to wear many different hats…
4.000: Sort of like an actual magician who has many tricks hidden in his sleeves.
4.000: There is something else about you, {{name}}…{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeIn: true} {{/fn}}
3.000: You are a visionary who has the capacity to bring real change to the world.
5.000: {{name}}, I’m getting the sense that you’re going to live a long, and happy life.
4.000: And although success might not be at your feet right now…
4.000: Your riches and fulfilment are merely waiting for you.{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeOut: true} {{/fn}}
3.000: {{#fn}} {fn: 'confirmToContinue', type: 'ok', message: 'Would you like to learn more about your path to fulfilment?', button: 'Click here to continue'} {{/fn}}.
3.000: Even though you will triumph, your path to success will not come easy.
5.000: As a Magician, your biggest challenge lies within how critical you are of people.
5.000: This applies to your expectations of both others and yourself.
4.000: I’m sensing that you tend to struggle with disappointment.
4.000: People seem to constantly let you down, and more often than not…
4.000: You feel as if you never seem to be able to meet the expectations that you’ve set for yourself.
5.000: {{name}}, don’t let that discourage you.
3.000: The world needs your talents and unique abilities.
3.000: The world needs people like you to be in positions of power.
4.000: You are what keeps the world functioning.
2.000: You bring change and improvement into the world.
3.000: You do, however, need to understand that it’s important for you to be…
4.000: Less closed off and more connected with others.
3.000: There is no doubt that people have a great amount of respect for you…
3.000: But at times you can appear aloof and difficult to speak with.
4.000: That way, you’ll be able to build trust and create even more fruitful interactions.
4.000: After all, {{name}}, conversation might not be your forte.
5.000: But mastering the art of conversation will eventually create opportunities for you.
4.000: {{name}}, I can already tell that your friends and family really look up to you.
6.000: You’re the one who they come to for detailed advice, a listening ear, or just a quick chat.
5.000: You’re not the type of person to have huge circles of friends.
5.000: You’re not someone who has trouble with making friends.
3.000: In fact, you have no trouble at all when it comes to…
3.000: Impressing others with your intelligence and sweet nature.
3.000: But you’re extremely selective with who you choose to let into your life.
4.000: You prefer to keep your loved ones close, and your wolf pack small.
4.000: {{name}}, you are incredibly nurturing.
4.000: You possess the unique ability to connect with others and blend into any social group.
6.000: You have a great deal to offer to this world.
3.000: {{name}}, you are a natural-born problem-solver.
3.000: I sense that you will find success and fulfilment as…
3.000: An entrepreneur, a manager, a journalist or a professor.
4.000: You can be very protective, {{name}}.
3.000: Especially as a parent.
1.000: You love your children, friends, and family fiercely.
4.000: You are willing to give and sacrifice everything that you have for them.
4.000: {{name}}, you are going to make a lasting mark in your lifetime.
5.000: You’re able to look at the world from a broad and altruistic perspective.
4.000: You see the big picture, and you have what it takes to make this world more connected.
4.000: {{name}}, you are a fast learner.
4.000: While others seem to find it difficult to even understand the fundamentals of a new skill…
5.000: You’re able to attain mastery over anything you set your mind to.
3.000: In a way, your greatest strength is your ability to learn.
4.000: You excel at one-on-one interactions…
3.000: And never fail to make an excellent impression on others.
3.000: Your charisma and eloquence intrigues people.
4.000: Ultimately, {{name}}, you are a realist.
3.000: You have a clear conscience.
2.000: Discerning right from wrong has never been a difficult task for you.
4.000: I’m getting the sense that you’re not too concerned about monetary riches…
4.000: Although you do wish you had more wealth.
3.000: There is, however, a greater purpose for you in this lifetime, {{name}}.
4.000: You must also be aware of the relationships that you forge…
3.000: And prevent yourself from succumbing to the expectations of others.
4.000: {{name}}, you can at times depend on the approval of others for your own happiness.
5.000: And that is something that you need to be careful of.
3.000: At times, it might feel as if you’re living your life for others.
4.000: That’s not what you want, {{name}}.
2.000: You want to live life for yourself.
3.000: You want to love yourself.
2.000: When you’re able to do that, success and the life of your dreams will follow.
5.000: And that’s what Individuation aims to teach you.
4.000: But there’s more to what I’ve learned about you, {{name}}…
3.000: It feels to me that you’re a deeply emotional and spiritual person.{{#fn}} {fn: 'sliderImageSpiritual', fadeIn: true} {{/fn}}
5.000: You’re connected to your soul in a way that’s far more profound than others.
4.000: Your thought process does not follow the rules and logic of the world.
5.000: In fact, I get the sense that you have quite the imagination.
3.000: Your thoughts and dreams are vivid, colourful…
3.000: And symbolic of who you are as an individual.{{#fn}} {fn: 'sliderImageSpiritual', fadeOut: true} {{/fn}}
3.000: {{name}}, you’re not someone who likes to argue.
5.000: You tend to avoid conflict and you’re uncanny when it comes to devising win-win situations.
5.000: But when backed into a corner, you’re not one to back down.
4.000: You fight fiercely for your loved ones and for righteousness.
4.000: You’re the first person to step in when you see a fight breaking out.
5.000: You’re the first person to reach for your pockets in the sight of a struggling busker.
4.000: You’re the first person your friends call when they’ve had a horrible day.
4.000: You’re the one who people rely on and look up to.
4.000: {{name}}, you are the role model of the world.
2.000: But every magician needs a side-kick…
4.000: A partner in crime who will stick by them when the going gets tough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeIn: true} {{/fn}}
3.000: This little chart I’ve prepared represents the archetypes that you’re most likely to end up with.
6.000: Or if you’re already in a committed relationship…
3.000: There’s a really good chance that your spouse is one of those archetypes.
3.000: {{name}}, you’re not the kind of person that has trouble when it comes to love.
5.000: People find themselves drawn to your charisma and kind nature.
4.000: You’re sort of a rare breed when it comes to love.
3.000: You communicate better than anyone else, and you’re an incredibly thoughtful individual.
5.000: The relationships that you forge with potential lovers are always profound and deep.
5.000: You become very selective with who you choose to date…
3.000: And who you choose to end up with…
2.000: For good reason, of course.
2.000: I sense that in the coming years, you will be the happiest you’ve ever been in that regard.
5.000: {{name}}, I’m sensing that your love life isn’t going to be the smoothest…
4.000: But you will find that happiness that you’re looking for soon enough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeOut: true} {{/fn}}
4.000: But don’t forget, {{name}}…
2.000: How you perceive yourself should not be determined by your love life.
5.000: Your spouse or partner is meant to play a supporting role in your life…
3.000: Not the lead.
2.000: Focus on your own personal growth, and I promise you, {{name}}…
4.000: You will soon discover heavenly rewards.