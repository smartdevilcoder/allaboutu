3.000: Hello {{name}},
3.000: If you’ve made it here, then congratulations!
3.000: Because this means that your archetype is the Jester!{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/jester.png', fadeIn: true} {{/fn}}
3.000: But what does this actually mean?
3.000: Don’t worry, everything you need to know will be on this page.
3.000: Stick to the end of it and I promise that you’ll experience something truly magical.{{#fn}} {fn: 'sliderImageSingle', path:'images/archetype-icons/jester.png', fadeOut: true} {{/fn}}
6.000: The quiz you’ve just taken is designed to identify your archetype…
5.000: A personality profile that’s broken down your entire being.
4.000: Your tendencies, your strengths, your past, and your future.
6.000: Your archetype takes into account your gender, your marital status…
5.000: How you answered the quiz, and how long you took to get those answers.
4.000: That’s right, {{name}}.
2.000: You can use that information to make astronomical changes in your life.
6.000: You’ll be able to leverage on your strengths, improve on your weaknesses…
4.000: And embark on the life that you’ve always dreamed of.
4.000: But before we get into that, I want to share with you about your archetype…
5.000: The Jester, and what it means.
3.000: {{name}}, you are truly a unique individual.
3.000: And I genuinely believe that this world needs more people like you.
5.000: You are an incredibly sharp and deeply intelligent being…
4.000: Not necessarily in the sense of academics…
2.000: But in terms of approaching various topics from a myriad of perspectives.
6.000: You’re different that way, {{name}}.
3.000: While the world walks in one direction, you have no qualms with going against it.
5.000: That’s what makes you so special, {{name}}.
4.000: You seem to be able to breeze your way through life…
4.000: With your bright thoughts and your hilarious interpretations.
4.000: That’s because as a Jester, you are highly intuitive.
4.000: Your sense of humour is constructed with creativity, and seasoned with your charisma.
6.000: {{name}}, you might not know it yet…
3.000: But you can be extremely charismatic…
3.000: And people fall in love with the conversations that they have with you.
4.000: You appear to be a natural-born performer…
3.000: And you’re always happy to be in the limelight and the life of the party.
5.000: There is something else about you, {{name}}…{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeIn: true} {{/fn}}
3.000: You put the smile in happiness.
4.000: {{name}}, I’m getting the sense that you’re going to live a long, and happy life.
6.000: And although success might not be at your feet right now...
3.000: Your riches and fulfilment are merely waiting for you.{{#fn}} {fn: 'sliderImageSingle', path:'images/script-images/globe.png', fadeOut: true} {{/fn}}
5.000: {{#fn}} {fn: 'confirmToContinue', type: 'ok', message: 'Would you like to learn more about your path to fulfilment?', button: 'Click here to continue'} {{/fn}}.
3.000: Even though you will triumph, your path to success will not come easy.
5.000: Talking to people is never a problem for you…
4.000: And I’m getting the sense that you tend to struggle...
2.000: When it comes to holding serious conversations.
4.000: Your nature revolves so much around light-hearted jokes and innocent conversations…
6.000: That touching on serious topics becomes a chore.
4.000: {{name}}, don’t let that discourage you.
3.000: The world needs your light-hearted and comical nature.
3.000: The world needs people like you to be in positions of power.
5.000: You are what keeps the world grounded.
3.000: It has been said, that even kings take the advice of their jesters.
4.000: You do, however, need to understand that it’s important for you…
4.000: To become more in touch with your inner self and inner emotions.
4.000: That way, you’ll be able to discover the deeper meanings…
4.000: Behind your personality and your purpose in this lifetime.
6.000: After all, {{name}}, you have the capacity to bring happiness…
5.000: Not just to those around you, but to the world.
4.000: And I believe that’s where you will find fulfilment.
4.000: {{name}}, I can already tell that your friends and family really adore being around you.
7.000: You’re the one who they come to for a wonderful time…
2.000: A light-hearted conversation, or just for a quick chat.
7.000: You’re not the type of person to have huge circles of friends.
4.000: You’re not someone who has trouble with making friends.
4.000: In fact, you have no trouble at all when it comes to…
4.000: Impressing others with your charisma and easy-going nature.
4.000: But you’re extremely selective when it comes to who you choose to let into your life.
5.000: You prefer to keep your loved ones close, and your wolf pack small.
5.000: {{name}}, you are incredibly impressionable.
4.000: You possess the unique ability to impress others and blend into any social group.
6.000: While others struggle to have their names remembered…
4.000: You seem to be the type of person who everyone knows.
3.000: You have a great deal to offer to this world.
3.000: {{name}}, you are capable of bringing smiles to the darkest of times.
6.000: I sense that you will find success and fulfilment as…
4.000: An entertainer, a performer, a salesman, or a writer.
9.000: I can tell that you’re not the protective type, {{name}}.
3.000: Even as a parent.
3.000: You prefer to give others the freedom and room that they need to grow on their own.
5.000: However, you must remember to be nurturing and present in the lives of your loved ones.
5.000: You are willing to give and sacrifice everything that you have for the people closest to you.
6.000: {{name}}, you are going to make a lasting mark in your lifetime.
6.000: You’re able to create hope from desolation.
3.000: Your unique ability to create happiness makes you an excellent catalyst of change.
6.000: You see the big picture, and you have what it takes to make this world a happier place…
6.000: One smile at a time.
3.000: {{name}}, you are an enthusiastic and humorous being.
3.000: Like a fairy godmother with the touch of your wand…
3.000: You’re able to spark laughter and smiles from all sorts of people.
5.000: You excel at interactions with both individuals and groups…
5.000: And never fail to make an excellent impression on others.
4.000: Your charisma and eloquence intrigues people.
4.000: Ultimately, {{name}}, you are a realist.
4.000: You understand the importance of money…
2.000: But I’m getting the sense that you’re not too concerned about monetary riches.
3.000: Although you do often wish that you had more wealth.
4.000: There is, however, a greater purpose for you in this lifetime, {{name}}.
4.000: Of course, {{name}}, you can at times be dependent on your ability to…
5.000: Make others laugh for your own happiness. And that is something you need to be careful of.
5.000: At times, you might feel as if you’re living your life for others.
4.000: That’s not what you want, {{name}}.
3.000: You want to live life for yourself.
2.000: You want to love yourself.
1.000: When you’re able to do that, success and the life of your dreams will follow.
6.000: And that’s what Individuation aims to teach you.
3.000: But there’s more to what I’ve learned about you, {{name}}…
6.000: It feels to me that you might not be the most emotional and spiritual person.{{#fn}} {fn: 'sliderImageSpiritual', fadeIn: true} {{/fn}}
6.000: But you’re intimately connected to your soul in a way that’s far more profound than others.{{#fn}} {fn: 'sliderImageSpiritual', fadeOut: true} {{/fn}}
5.000: Your thought process does not follow the rules and logic of the world.
4.000: In fact, I get the sense that you have quite the imagination.
5.000: Your thoughts and dreams are vivid, colourful…
4.000: And symbolic of who you are as an individual.
3.000: {{name}}, you’re not someone who likes to argue.
3.000: You tend to avoid conflict and you’re uncanny when it comes to devising win-win situations.
7.000: But when backed into a corner, you’re not one to back down.
4.000: You fight fiercely for your loved ones and for righteousness.
6.000: You’re the first person to step in when you see a fight breaking out.
5.000: You’re the first person to reach for your pockets in the sight of a struggling busker.
4.000: You’re the first person your friends call when they’ve had a horrible day.
4.000: You’re the one who people rely on and look up to.
4.000: {{name}}, you are the light of hope and happiness of the world.
5.000: But every superhero needs a side-kick…
3.000: A partner in crime who will stick by them when the going gets tough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeIn: true} {{/fn}}
5.000: This little chart I’ve prepared represents the archetypes that you’re most likely to end up with.
5.000: Or if you’re already in a committed relationship…
3.000: There’s a really good chance that your spouse is one of those archetypes.
4.000: {{name}}, you’re not the kind of person that has trouble when it comes to love.
6.000: People find themselves drawn to your charisma and kind nature.
4.000: You’re sort of a rare breed when it comes to love.
4.000: You communicate better than anyone else. You’re an incredibly thoughtful individual.
5.000: The relationships that you forge with potential lovers are always profound and deep.
9.000: You become very selective when it comes to who you choose to date…
4.000: And who you choose to end up with…
2.000: For a good reason, of course.
2.000: I sense that in the coming years, you will be the happiest you’ve ever been in that regard.
7.000: {{name}}, I’m sensing that your love life isn’t going to be the smoothest…
5.000: But you will find the happiness that you’re looking for soon enough.{{#fn}} {fn: 'sliderImageLoveCompat', fadeIn: true} {{/fn}}
7.000: But don’t forget, {{name}}…
2.000: How you perceive yourself should not be determined by your love life.
5.000: Your spouse or partner is meant to play a supporting role in your life…
4.000: Not the lead.
2.000: Focus on your own personal growth, and I promise you, {{name}}…
4.000: You will soon discover heavenly rewards.