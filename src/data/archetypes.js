module.exports = {
  caregiver: {
    title: 'The Caregiver',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1861270524',
        listname: 'awlist4379647',
      },
      affiliate: {
        meta_web_form_id: '703433977',
        listname: 'awlist4804736',
      }
    },
    clickbank: {
      link: 'http://dar-car.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28769'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=caregiveroptin2',
    },
  },
  creator: {
    title: 'The Creator',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '123025483',
        listname: 'awlist4379660',
      },
      affiliate: {
        meta_web_form_id: '1459941350',
        listname: 'awlist4804749',
      }
    },
    clickbank: {
      link: 'http://dar-cre.individua1.pay.clickbank.net/?cbskin=17588&cbfid=27121'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=creatoroptin2',
    },
  },
  explorer: {
    title: 'The Explorer',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '749843416',
        listname: 'awlist4380223',
      },
      affiliate: {
        meta_web_form_id: '2093413690',
        listname: 'awlist4804761',
      }
    },
    clickbank: {
      link: 'http://dar-exp.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28787'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=exploreroptin2',
    },
  },
  hero: {
    title: 'The Hero',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '62483841',
        listname: 'awlist4378398',
      },
      affiliate: {
        meta_web_form_id: '100773792',
        listname: 'awlist4804773',
      }
    },
    clickbank: {
      link: 'http://dar-her.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28788'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=herooptin2',
    },
  },
  innocent: {
    title: 'The Innocent',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '965919118',
        listname: 'awlist4380218',
      },
      affiliate: {
        meta_web_form_id: '1963606102',
        listname: 'awlist4804781',
      }
    },
    clickbank: {
      link: 'http://dar-inn.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28789'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=innocentoptin2',
    },
  },
  jester: {
    title: 'The Jester',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1258897520',
        listname: 'awlist4380220',
      },
      affiliate: {
        meta_web_form_id: '959437341',
        listname: 'awlist4810136',
      }
    },
    clickbank: {
      link: 'http://dar-jes.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28790'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=jesteroptin2',
    },
  },
  lover: {
    title: 'The Lover',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1085563446',
        listname: 'awlist4380211',
      },
      affiliate: {
        meta_web_form_id: '891115309',
        listname: 'awlist4810143',
      }
    },
    clickbank: {
      link: 'http://dar-lov.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28791'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=loveroptin2',
    },
  },
  magician: {
    title: 'The Magician',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '769616786',
        listname: 'awlist4379665',
      },
      affiliate: {
        meta_web_form_id: '2133059785',
        listname: 'awlist4810151',
      }
    },
    clickbank: {
      link: 'http://dar-mag.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28792'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=memberoptin2',
    },
  },
  member: {
    title: 'The Member',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1446575107',
        listname: 'awlist4379641',
      },
      affiliate: {
        meta_web_form_id: '1976529073',
        listname: 'awlist4810158',
      }
    },
    clickbank: {
      link: 'http://dar-mem.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28793'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=magicianoptin2',
    },
  },
  outlaw: {
    title: 'The Outlaw',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1844169178',
        listname: 'awlist4380228',
      },
      affiliate: {
        meta_web_form_id: '1312139852',
        listname: 'awlist4810163',
      }
    },
    clickbank: {
      link: 'http://dar-out.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28794'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=outlawoptin2',
    },
  },
  ruler: {
    title: 'The Ruler',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '293430144',
        listname: 'awlist4378395',
      },
      affiliate: {
        meta_web_form_id: '704001842',
        listname: 'awlist4810177',
      }
    },
    clickbank: {
      link: 'http://dar-rul.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28795'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=ruleroptin2',
    },
  },
  sage: {
    title: 'The Sage',
    aweber: {
      default: {
        meta_adtracking: 'Quiz_Results_Opt_In',
      },
      facebook: {
        meta_web_form_id: '1698452574',
        listname: 'awlist4380225',
      },
      affiliate: {
        meta_web_form_id: '1208503349',
        listname: 'awlist4810184',
      }
    },
    clickbank: {
      link: 'http://dar-sag.individua1.pay.clickbank.net/?cbskin=17588&cbfid=28797'
    },
    clickmagick: {
      imgSrc: 'http://www.clkmg.com/api/a/pixel/?uid=59760&att=2&ref=sageoptin2',
    },
  },
};
