import URL from 'url';
import {render, h } from 'preact';
import GoogleTagManager from 'react-gtm-module';
import pkg from '../package.json';
import config from '../config.json';
import store from './store';

import 'nunito-fontface';
// import 'bootstrap/dist/css/bootstrap-reboot.css';

window.isLocalhost = Boolean(!location.hostname || location.hostname.match(/localhost|(127|192)\.[0-9.]+|.dev$/));
window.isGithub = Boolean(location.hostname.match(/github.io/));
window.isDev = window.isLocalhost || window.isGithub;
window.url = URL.parse(String(location), true);
if ('followup' in window.url.query) {
	window.url.query['follow-up'] = window.url.query.followup;
}
window.cleanUrl = URL.format(Object.assign({}, window.url, {
	query: {},
	search: null
}));
window.originalTitle = document.title;

if (window.isDev) {
	console.log('Dev mode', window.isLocalhost, window.isGithub);
	console.log('v' + pkg.version);
	try {
		console.log('Last commit(s):', window.lastCommit);
	} catch (error) {
		console.error(error);
	}
}

if (!window.isDev) {
	window.history.replaceState(null, null, window.cleanUrl);
}

const appEl = document.getElementById('app');
store.ready.then((data = {}) => {
	data.urlQuery = url.query || {};
	const App = require('./components/app').default;
	window.reload = () => render(h(App, data), appEl, appEl.lastChild);
	if (!window.isDev) {
		GoogleTagManager.initialize({
			gtmId: config['google-tag-manager'].id
		});
	}
	render(h(App, data), appEl, appEl.lastChild);
}).catch(error => {
	console.error(error);
	const msg = isLocalhost ? error.stack || error.message || error : error.message || error;
	render(h('pre', {}, [msg]), appEl, appEl.lastChild);
}).then(() => {
	appEl.classList.remove('loading');
});

if (module.hot) {
	module.hot.accept();
}
