import localforage from 'localforage';
import { version } from '../package';

localforage.config({ name: 'v' + version });

export default {
  ready: localforage.getItem('data').then(data => {
    console.log(`Retrieved store data:`, data);
    return data || {};
  }),
  save: data => {
    delete data.class;
    console.log(`Saved store data:`, data);
    const filterKeys = ['quizData', 'formData'];
    const filteredData = Object.keys(data)
      .filter(k => filterKeys.includes(k))
      .reduce((_, k) => Object.assign(_, {
        [k]: data[k]
      }), {});
    return localforage.setItem('data', filteredData);
  },
  clear: () => localforage.setItem('data', {}),
}
