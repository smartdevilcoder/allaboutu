<?php /*
  Template Name: Whats your archetype v2
  Author: Freeth <freethlua@gmail.com>
*/ ?>

<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>What's Your Archetype? | Individualogist</title>
  </head>
  <body>
    <div id="whats-your-archetype_app"></div>
    <script src='/wp-content/themes/individualogist/whats-your-archetype/build/app.js' async></script>
    <script src="//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=247959338893932" async></script>
  </body>
</html>
