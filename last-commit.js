const { execSync } = require('child_process');

module.exports = JSON.stringify(execSync('git log -5 --pretty=oneline --abbrev-commit')
  .toString()
  .trim()
  .split(/[\n\r]+/g)
  .map(_ => _.trim())
  .slice(0, 3)
  .join(', '))
